package mvp_sqlite.training.com.firstmvpnotebook.db.tables;


public interface Notes {

    String TABLE_NAME= "table_name";

    interface Columns{
        String NOTE_ID = "_id";
        String NOTE_TEXT = "note_text";
    }
}
