package mvp_sqlite.training.com.firstmvpnotebook.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import mvp_sqlite.training.com.firstmvpnotebook.db.tables.Notes;


public class DbHelper extends SQLiteOpenHelper {

    private static String DB_NAME = "database.db";
    private static int DB_VERSION = 1;

    public DbHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + Notes.TABLE_NAME
                + " ("
                + Notes.Columns.NOTE_ID + " integer primary key,"
                + Notes.Columns.NOTE_TEXT + " text"
                + ");"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "
                + Notes.TABLE_NAME
                + ";"
        );
        onCreate(db);
    }
}
