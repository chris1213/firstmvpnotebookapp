package mvp_sqlite.training.com.firstmvpnotebook.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import mvp_sqlite.training.com.firstmvpnotebook.pojo.Note;
import mvp_sqlite.training.com.firstmvpnotebook.mvp.NotesInteractorInterface;
import mvp_sqlite.training.com.firstmvpnotebook.db.tables.Notes;


public class NoteDAO implements NotesInteractorInterface {

    private DbHelper dbHelper;

    public NoteDAO(Context context) {
        dbHelper = new DbHelper(context);
    }


    @Override
    public void insertNote(Note note) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(Notes.Columns.NOTE_TEXT, note.getNoteText());
        dbHelper.getWritableDatabase().insert(Notes.TABLE_NAME, null, contentValues);
    }

    @Override
    public void removeNoteById(int id) {
        SQLiteDatabase sqliteHelper = dbHelper.getWritableDatabase();
        sqliteHelper.delete(
                Notes.TABLE_NAME,
                Notes.Columns.NOTE_ID+"=?",
                new String[]{String.valueOf(id)}
        );
    }

    @Override
    public void updateNote(Note note) {
        SQLiteDatabase sqliteHelper = dbHelper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Notes.Columns.NOTE_TEXT, note.getNoteText());
        sqliteHelper.update(
                Notes.TABLE_NAME,
                contentValues,
                Notes.Columns.NOTE_ID+"=?",
                new String[]{String.valueOf(note.getId())}
                );
    }

    @Override
    public void getAllNotes(OnFinishedListener listener) {
        listener.sendListOfValues(allNotesList());
    }

    @Override
    public List<Note> getAllNotes() {
        return allNotesList();
    }

    private List<Note> allNotesList() {
        Cursor cursor;
        cursor = dbHelper.getReadableDatabase().query(
                Notes.TABLE_NAME,
                new String[]{Notes.Columns.NOTE_ID, Notes.Columns.NOTE_TEXT},
                null, null, null, null, null
        );

        List<Note> listOfDbResults = new ArrayList<Note>();
        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                listOfDbResults.add(mapCursorToNote(cursor));
            }
        }
        return listOfDbResults;
    }

    private Note mapCursorToNote(Cursor cursor) {
        Note note = new Note();
        int columnIndexID = cursor.getColumnIndex(Notes.Columns.NOTE_ID);
        int columnIndexText = cursor.getColumnIndex(Notes.Columns.NOTE_TEXT);
        int id = cursor.getInt(columnIndexID);
        String text = cursor.getString(columnIndexText);

        note.setId(id);
        note.setNoteText(text);
        return note;
    }
}
