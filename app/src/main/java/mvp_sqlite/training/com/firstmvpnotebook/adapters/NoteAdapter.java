package mvp_sqlite.training.com.firstmvpnotebook.adapters;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import mvp_sqlite.training.com.firstmvpnotebook.R;
import mvp_sqlite.training.com.firstmvpnotebook.pojo.Note;

public class NoteAdapter extends ArrayAdapter<Note>{

    public NoteAdapter(@NonNull Context context, @NonNull List<Note> objects) {
        super(context, 0, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null){
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder)convertView.getTag();
        }

        Note note = getItem(position);
        viewHolder.textAdapter.setText(note.getNoteText());

        return convertView;
    }

    static class ViewHolder{
        @BindView(R.id.text_adapter)
        TextView textAdapter;

        ViewHolder(View view){
            ButterKnife.bind(this, view);
        }
    }
}
