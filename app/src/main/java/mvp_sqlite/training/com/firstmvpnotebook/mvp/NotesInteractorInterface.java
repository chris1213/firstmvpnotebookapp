package mvp_sqlite.training.com.firstmvpnotebook.mvp;


import java.util.List;

import mvp_sqlite.training.com.firstmvpnotebook.pojo.Note;

public interface NotesInteractorInterface {
    void insertNote(Note note);
    void removeNoteById(int id);
    void updateNote(Note note);
    List<Note> getAllNotes();

    interface OnFinishedListener{
        void sendListOfValues(List<Note> notesList);
    }
    void getAllNotes(OnFinishedListener listener);
}
